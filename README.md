# pra-formative4
Javascript & MySQL


1. Buat database formative4 & tables
- Membuat database dengan nama: formative4
- Membuat 3 tables di dalam database formative4, seperti berikut:
    1. Student (id, name, email, phone, birthdate)
    2. Course (id, name)
    3. Scoresheet (id, studentId, courseId)
- Simpan query pembuatan table diatas didalam file query1.txt
- Buat printscreens yang menampilkan 3 tables di atas

2. Buat database formative4 & tables
- Membuat query yang dapat menampilkan data seperti berikut (sebelum melakukan ini, pastikan ada data di dalam 3 tables yang sudah dibuat sebelumnya)
- Menampilkan:
    1. Semua students, course & score mereka dalam 1 query
    2. Menampilkan semua students yang nilai score mereka di atas angka 8

3. Javascript
- Membuat array yang berisi nama buah-buahan. Minimal 5
- Membuat loop (menggunakan for loop, for in dan for each) dan menampilkan isi dari buah-buahan yang dibuat sebelumnya
- Simpan code JavaScript ini dalam file formative3.js
